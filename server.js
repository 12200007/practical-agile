const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path: './config.env'})
const app = require('./app')


//MongodbAtlas
const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)
const local_DB = process.env.DATABASE_LOCAL

mongoose.connect(DB).then((con) => {
    console.log(con.connection)
    console.log('DB connection successful')
})

 const port = 4001
app.listen(port, () => {
    console.log(`App running on port ${port}..`)
})
